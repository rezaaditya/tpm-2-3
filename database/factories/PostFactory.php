<?php

use Faker\Generator as Faker;

$factory->define(App\Post::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(),
        'content' => $faker->text(),
        'category' => $faker->randomElement(
            array('food', 'news', 'health')
        )
    ];
});
