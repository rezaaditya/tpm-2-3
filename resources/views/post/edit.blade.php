<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Edit</title>
</head>
<body>
    <h1>Edit Post</h1>
    <form action="{{url('/post/'.$post->id.'/update')}}" method="post" enctype="multipart/form-data">
        @csrf
        <b>Title:</b> <input type="text" name="title" value="{{$post->title}}"><br>
        <b>Photo:</b> <input type="file" name="photo"><br>
        <b>Content:</b> <textarea name="content">{{$post->content}}</textarea><br>
        <b>Category:</b> <input type="text" name="category" value="{{$post->category}}">
        <input type="submit" value="submit">
    </form>
</body>
</html>