<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Create Post</h1>
    <form action="{{url('/post')}}" method="post" enctype="multipart/form-data">
        @csrf
        <b>Title:</b> <input type="text" name="title"><br>
        <b>Photo:</b> <input type="file" name="photo"><br>
        <b>Content:</b> <textarea name="content"></textarea><br>
        <b>Category:</b> <input type="text" name="category">
        <input type="submit" value="submit">
    </form>
    <h1>Post Lists</h1>
    <p><a href="">Hapus hobi basket</a></p>
    <table border="1">
        <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
                <th>Category</th>
                <th>Photo</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach($posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->content }}</td>
                <td>{{ $post->category }}</td>
                <td><img style="height:300px;" src="{{ asset('storage/'.$post->photo) }}"></td>
                <td><a href="{{ url('post/'.$post->id.'/edit') }}">Edit</a> | <a href="{{ url('post/'.$post->id.'/delete') }}">Delete</a></td>
            </tr>
            @endforeach
        </tbody>
    </table>
    <h1>Food Post Lists</h1>
    <table border="1">
        <thead>
            <tr>
                <th>Title</th>
                <th>Content</th>
                <th>Category</th>
            </tr>
        </thead>
        <tbody>
            @foreach($food_posts as $post)
            <tr>
                <td>{{ $post->title }}</td>
                <td>{{ $post->content }}</td>
                <td>{{ $post->category }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>
</html>