<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/post', 'PostController@index');
Route::post('/post', 'PostController@store');
Route::post('/post/{id}/update', 'PostController@update');
Route::get('/post/{id}/delete', 'PostController@destroy');
Route::get('/post/{id}/edit', 'PostController@edit');
Route::get('/post/deletebasket', 'PostController@deletebasket');
